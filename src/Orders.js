import React, { Component } from 'react';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import Badge from '@material-ui/core/Badge';
import Modal from '@material-ui/core/Modal';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import CloseIcon from '@material-ui/icons/Close';
import TableFooter from '@material-ui/core/TableFooter';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';


function rand() {
	return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
	const top = 0;
	const left = 0;

	return {
		top: `${top}%`,
		left: `${left}%`,
		transform: `translate(-${top}%, -${left}%)`,
		overflow: 'scroll',
		height: '100%',
		display: 'block'
	};
}

const useStyles = makeStyles((theme) => ({
	paper: {
		position: 'absolute',
		width: '100%',
		backgroundColor: theme.palette.background.paper,
		border: '2px solid #000',
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3)
	}
}));

export default function Orders(props) {
	const classes = useStyles();
	// getModalStyle is not a pure function, we roll the style only on the first render
	const [ modalStyle ] = React.useState(getModalStyle);
	const [ open, setOpen ] = React.useState(false);

	let uniqueKey = 0;
	let suma = 0;

	// const [ productList, setProductList ] = React.useState([
	// 	{
	// 		numer: 'Brak numeru',
	// 		nazwa: 'Brak nazwy',
	// 		cena: 'Brak ceny'
	// 	}
	// ]);

	const handlePOST = (formularz) => {
		let dane = [ props.lista, formularz ];
		console.log('Zamówienie wysłano!', dane);

		// fetch('http://localhost:3001/postDataZamowienia', {
		// 	method: 'POST',
		// 	mode: 'cors',
		// 	body: {
		// 		dane: dane
		// 	}
		// }).then((response) => {
		// 	let json = response.json();
		// }).then(json => {
		//     console.log(json);
		// });

		async function postData(url = 'http://localhost:3001/postDataZamowienia', data = dane) {
			// Default options are marked with *
			const response = await fetch(url, {
				method: 'POST', // *GET, POST, PUT, DELETE, etc.
				mode: 'cors',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(data) // body data type must match "Content-Type" header
			});

			return console.log(response); // parses JSON response into native JavaScript objects
		}

		postData();
	};

	const handleOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const handleClear = () => {
		props.zmienListe([]);
		setOpen(false);
	};

	const handleOrder = () => {
		if (
			props.lista.length > 0 &&
			document.getElementById('Z_ADRES').value !== '' &&
			document.getElementById('Z_TELEFON').value !== ''
		) {
			console.log('Spakuj zamówienie..', props.lista);

			let dane = {
				imieinazwisko: document.getElementById('Z_IMIENAZWISKO').value,
				email: document.getElementById('Z_EMAIL').value,
				telefon: document.getElementById('Z_TELEFON').value,
				adres: document.getElementById('Z_ADRES').value
			};

			console.log('=======================================');
			console.log('Formularz wysłany!!', dane);

			document.getElementById('Z_IMIENAZWISKO').value = '';
			document.getElementById('Z_EMAIL').value = '';
			document.getElementById('Z_TELEFON').value = '';
			document.getElementById('Z_ADRES').value = '';

			handlePOST(dane);
			handleClear();
		} else {
			alert('Brak produktów lub nieprawidłowo uzupełniony formularz!');
		}
	};

	const body = (
		<div style={modalStyle} className={classes.paper}>
			<h2 id="simple-modal-title">Twoje zamówienie</h2>
			<CloseIcon onClick={handleClose} style={{ position: 'absolute', top: 10, right: 10, cursor: 'pointer' }} />
			<TableContainer component={Paper}>
				<Table className={classes.table} aria-label="simple table">
					<TableHead>
						<TableRow>
							<TableCell align="left">Nazwa</TableCell>

							<TableCell align="right">Typ</TableCell>
							<TableCell align="right">Cena</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{props.lista.map((row) => {
							uniqueKey++;
							suma += row.cena;
							return (
								<TableRow key={uniqueKey}>
									<TableCell align="left">{row.nazwa}</TableCell>
									<TableCell align="right">{row.typ + ' nr. ' + row.number}</TableCell>
									<TableCell align="right">{row.cena + ' PLN'}</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</TableContainer>

			<Grid container>
				<Grid item xs={12} sm={12}>
					<Paper>
						Adres dostawy:
						<form className={classes.root} noValidate autoComplete="off">
							<TextField style={{ width: '100%' }} id="Z_IMIENAZWISKO" label="Imię i nazwisko:" />
							<Divider />
							<TextField style={{ width: '100%' }} id="Z_EMAIL" label="E-mail:" />
							<Divider />
							<TextField style={{ width: '100%' }} id="Z_TELEFON" label="Telefon:" />
							<Divider />
							<TextField style={{ width: '100%' }} id="Z_ADRES" label="Adres:" />
							<Divider />
						</form>
					</Paper>
				</Grid>
				<Grid item xs={12} sm={5}>
					<Button variant="contained" color="secondary" style={{ width: '100%' }} onClick={handleClear}>
						Wyszyść
					</Button>
				</Grid>

				<Grid item xs={12} sm={5}>
					<Button variant="contained" color="primary" style={{ width: '100%' }} onClick={handleOrder}>
						Zamów
					</Button>
				</Grid>
				<Grid item xs={12} sm={2} style={{ width: '100%', textAlign: 'right', fontSize: '3vh' }}>
					<div>SUMA: {suma + ' PLN'}</div>
				</Grid>
			</Grid>
		</div>
	);

	return (
		<div>
			<React.Fragment>
				<IconButton
					onClick={handleOpen}
					style={{ position: 'fixed', margin: '15px 5px 5px 5px' }}
					color="default"
					aria-label="shop"
					component="span"
				>
					<Badge badgeContent={props.lista.length} color={'primary'}>
						<ShoppingCartIcon />
					</Badge>
				</IconButton>
			</React.Fragment>
			<Modal
				open={open}
				onClose={handleClose}
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
			>
				{body}
			</Modal>
		</div>
	);
}
