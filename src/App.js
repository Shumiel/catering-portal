/**
 * importy umożliwiające interakcję z obrazkami, stylami i skryptami z innych plików, w obrębie własnie App.js
 */
import logo from "./logo.svg";
import "./App.css";
import React, { useState, useEffect } from "react";
//import komponentu odpowiedzialnego za zamówienia
import Orders from "./Orders.js";
//import komponentu imitującego panel administratora
import Panel from "./Panel.js";
//momentjs, zastępuje new Date w JS, jest wygodniejszy
import moment from "moment";
//importy materialUi[CSS]
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import TextField from "@material-ui/core/TextField";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import { red } from "@material-ui/core/colors";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import Button from "@material-ui/core/Button";
import CallReceivedIcon from "@material-ui/icons/CallReceived";
//routers - przekierowania stron/komponentów
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { ShoppingCart } from "@material-ui/icons";

//style CSS, aplikowane w sposób przedstawiony w MaterialUI docs, pozwala to na implementację dynamicznych styli wewnątrz pliku .js (bez App.css, mało go użyto w projekcie)
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    background: "#e4e4e4",
  },
  inputFields: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
  listMenu: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: "#e4e4e4",
  },
  home: {
    width: "100%",
  },
  rootCard: {
    maxWidth: "255px",
    minWidth: "255px",
    minHeight: "430px",
    marginTop: 10,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

/**
 * Definicja komponentu App, w którym mówimy React'owi, aby załadował fragment kodu który,
 * generuje DOM (HTML i CSS), a następnie tworzy logikę dla wygenerowanego komponentu za pomocą mechanizmów react (stany, efekty).
 */
function App() {
  const [count, setCount] = useState(0); //definicja stanu w hook'ach(czyli komponentach) - jest to zmienna[count] z funckją[setCount] odpowiedzialną za nadanie wartości
  const classes = useStyles();
  const [selectedDate, setSelectedDate] = React.useState(moment().format());
  const [convertedDate, setConvertedDate] = React.useState(0);
  const [contactForm, setContactForm] = React.useState(0);

  const [productList, setProductList] = React.useState([]);

  const handleProductClick = (data) => {
    setProductList([...productList, data]);
    console.log("handleProductClick - productList - data:", productList, data);
  };

  //funkcja obsługująca wywołanie zmiany daty, uaktualnia stan selectedDate
  const handleDateChange = (date) => {
    setSelectedDate(date);
    setConvertedDate(moment(selectedDate).format("DD-MM-YYYY"));
  };
  //funkcja obsługująca wywołanie kliknięcia formularza
  const handleFormClick = (event) => {
    let dane = {
      imieinazwisko: document.getElementById("IMIENAZWISKO").value,
      email: document.getElementById("EMAIL").value,
      telefon: document.getElementById("TELEFON").value,
      dodatkoweinfo: document.getElementById("DODATKOWEINFO").value,
    };
    setContactForm(dane);
    console.log("=======================================");
    console.log("Formularz wysłany!!", dane);

    document.getElementById("IMIENAZWISKO").value = "";
    document.getElementById("EMAIL").value = "";
    document.getElementById("TELEFON").value = "";
    document.getElementById("DODATKOWEINFO").value = "";

    async function postData(
      url = "http://localhost:3001/postDataZgloszenia",
      data = dane
    ) {
      // Default options are marked with *
      const response = await fetch(url, {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data), // body data type must match "Content-Type" header
      });

      return console.log(response); // parses JSON response into native JavaScript objects
    }

    postData();
  };

  useEffect(() => {
    // Fragment kodu wywoływany w przypadku każdej akcji / aktualizacji stanu
    console.log("Odświeżono komponent App! Wczytuję zmiany...");
    console.log("selectedDate", selectedDate);
    console.log("convertedDate", convertedDate);
  });

  useEffect(() => {
    //Fragment kodu wywoływany w przypadku każdej aktualizacji stanu count
  }, [count]); // Uruchom ponownie efekt tylko wtedy, gdy zmieni się wartość count

  useEffect(() => {
    //Fragment kodu wywoływany tylko za pierwszym załadowaniem strony>komponentu
    console.log("Jednorazowy efeky został wywołany!");
    document.title = `Catering - Paweł`;
  }, []); // Uruchom efekt tylko raz

  //wygeneruj szablon strony
  //<Router> jest elementem biblioteki react-router, służy do przeładowywania widoków strony (zmienianie kart)
  return (
    <Router>
      <Orders lista={productList} zmienListe={setProductList} />

      <CssBaseline />
      <Container fixed>
        <div className={classes.root}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Paper className={classes.paper}>
                <img
                  src={process.env.PUBLIC_URL + "/logo_transparent.png"}
                  alt={"logo"}
                  width={"400vh"}
                  height={"350vh"}
                  style={{ border: "0", margin: "-95px", paddingTop: "5px" }}
                />
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper}>
                <Grid container direction="column" justify="space-around">
                  <List
                    component="nav"
                    className={classes.listMenu}
                    aria-label="mailbox folders"
                  >
                    <Link
                      to="/"
                      style={{ textDecoration: "none", color: "black" }}
                    >
                      <ListItem button>Strona Główna</ListItem>
                    </Link>
                    <Divider />
                    <Link
                      to="/sniadania"
                      style={{ textDecoration: "none", color: "black" }}
                    >
                      <ListItem button divider>
                        Śniadania
                      </ListItem>
                    </Link>

                    <Link
                      to="/obiady"
                      style={{ textDecoration: "none", color: "black" }}
                    >
                      <ListItem button>Obiady</ListItem>
                    </Link>
                    <Divider />
                    <Link
                      to="/koktajle"
                      style={{ textDecoration: "none", color: "black" }}
                    >
                      <ListItem button>Koktajle</ListItem>
                    </Link>
                  </List>
                </Grid>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={9}>
              <Paper className={classes.paper}>
                <Switch>
                  <Route exact path="/">
                    <Home />
                  </Route>
                  <Route path="/sniadania">
                    <Sniadania dodaj={handleProductClick} />
                  </Route>
                  <Route path="/obiady">
                    <Obiady dodaj={handleProductClick} />
                  </Route>
                  <Route path="/koktajle">
                    <Koktajle dodaj={handleProductClick} />
                  </Route>
                </Switch>
              </Paper>
            </Grid>

            <Grid item xs={6} sm={6}>
              <Paper className={classes.paper}>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <Typography className={classes.heading}>
                      Dane kontaktowe:
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Typography component={"span"} align={"left"}>
                      <Typography>plac Defilad 1, 00-901 Warszawa</Typography>
                      <Typography>(22) 19115</Typography>
                      <Typography>catering@gmail.com</Typography>
                    </Typography>
                  </AccordionDetails>
                </Accordion>
                <Divider />
                <iframe
                  src={
                    "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2443.644384100186!2d21.003639415796705!3d52.2316783797607!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x471ecc8c93a84a83%3A0xa2e1cc1f1b7198de!2sPa%C5%82ac%20Kultury%20i%20Nauki%2C%20plac%20Defilad%201%2C%2000-901%20Warszawa!5e0!3m2!1spl!2spl!4v1613389208128!5m2!1spl!2spl"
                  }
                  width={"100%"}
                  height={"450"}
                  style={{ border: "0" }}
                  allowFullScreen={""}
                  aria-hidden={"false"}
                  tabIndex={"0"}
                />
              </Paper>
            </Grid>
            <Grid item xs={6} sm={6}>
              <Grid container direction="column" justify="space-around">
                <Paper className={classes.paper}>
                  Formularz kontaktowy:
                  <form className={classes.root} noValidate autoComplete="off">
                    <TextField
                      style={{ width: "100%" }}
                      id="IMIENAZWISKO"
                      label="Imię i nazwisko:"
                    />
                    <Divider />
                    <TextField
                      style={{ width: "100%" }}
                      id="EMAIL"
                      label="E-mail:"
                    />
                    <Divider />
                    <TextField
                      style={{ width: "100%" }}
                      id="TELEFON"
                      label="Telefon:"
                    />
                    <Divider />
                    <TextField
                      style={{ width: "100%" }}
                      id="DODATKOWEINFO"
                      label="Dodatkowe informacje:"
                      multiline
                      rows={4}
                    />
                    <Divider />
                    <Button
                      style={{ margin: "5px", width: "100%" }}
                      variant="contained"
                      color="primary"
                      onClick={(event) => handleFormClick(event)}
                    >
                      Wyślij formularz kontaktowy
                    </Button>
                  </form>
                </Paper>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </Container>
    </Router>
  );
}

//export jest potrzebny, aby React wiedział w index.js jaki komponent ma zostać załdowany do cyklu życi aplikacji w dany widok
export default App;

//strona główna, komponent ładuje informacje o firmie
function Home() {
  const classes = useStyles();

  return (
    <div className={classes.home}>
      <Container align={"justify"}>
        <Typography variant="h5" component={"span"} gutterBottom>
          Trochę o nas
        </Typography>
        <Typography
          variant="subtitle1"
          component={"span"}
          display={"block"}
          gutterBottom
        >
          Historia firmy
        </Typography>
        <Typography variant="body1" gutterBottom>
          Przyjmuje się, że firmę utworzono już w roku 2018, gdy Pan Paweł
          postanowił udać się na swoje pierwsze studia o kierunku
          informatycznym. Długa i niełatwa droga walki na wspomnianym kierunku
          pokazała Panu Pawłu, że informatyka to ciężkie rzemiosło, które wymaga
          wielkiej wprawy i cierpliwości. Pod koniec swoich lat na uczelni
          poznał osoby zaznajomione z branżą, co pozwoliło mu na połączenie
          swoich umiejętności zawodowych wraz z zamiłowaniem do jedzenia - w
          efekcie czego powstała 'Firma Cateringowa - Paweł'.
        </Typography>
        <Typography variant="subtitle2" component={"span"} gutterBottom>
          Założenia firmy
        </Typography>
        <Typography variant="body2" gutterBottom>
          Celem firmy jest wyjście na przeciw oczekiwaniom wszelkich imprez i
          spotkań, wymagających od gospodarza zaangażowania w kwestii
          serwowanego jedzenia. Z naszego spcjalnego menu oferujemy śniadania,
          obiady i koktajle odpowiednie na każdą porę roku. Wierzmy, że każdy
          znajdzie w naszej ofercie posiłki warte uwagi.
        </Typography>
        <Typography variant="button" display="block" gutterBottom>
          Realizujemy zamówienia również dla osób indywidualnych.
        </Typography>
        <Typography variant="caption" display="block" gutterBottom>
          Zamówienia należy złożyć poprzez formularz kontaktowy lub korzystając
          z udostępnionego maila i numeru telefonu.
        </Typography>
        <Typography variant="overline" display="block" gutterBottom>
          Dane kontaktowe i formularz, znajdą Państwo na dole strony wraz z mapą
          lokalizacji.
        </Typography>
      </Container>
    </div>
  );
}

//komponent produktu, jest wykorzystywany w komponentach sniadanie, obiady i koktajle aby załadować wielokrotnie podobną strukturę danych
function Produkt(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const handleAddProductToCart = () => {
    console.log(props.typ + " nr." + props.number + " dodano do koszyka!");
    props.dodaj(props);
  };

  return (
    <Card className={classes.rootCard}>
      <CardHeader
        title={props.nazwa}
        subheader={props.typ + " nr." + props.number}
      />
      <CardMedia
        className={classes.media}
        image={process.env.PUBLIC_URL + props.obrazek}
        title={props.nazwa + " " + props.typ}
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {props.cena + "PLN"}
        </Typography>
      </CardContent>
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {props.opis}
          {/* This impressive paella is a perfect party dish and a fun meal to cook
          together with your guests. Add 1 cup of frozen peas along with the
          mussels, if you like. */}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        {/* <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton> */}
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="pokaż więcej"
        >
          <ExpandMoreIcon />
        </IconButton>
        <IconButton
          onClick={handleAddProductToCart}
          aria-label="dodaj do koszyka"
        >
          <ShoppingCart />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>Szczegółowy opis:</Typography>
          <Typography paragraph>{props.dlugiopis}</Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}

//komponent wyświetlający produkty ze śniadaniem
function Sniadania(props) {
  const [sniadaniaLista, setsniadaniaLista] = React.useState([
    {
      number: 1,
      typ: "Śniadanie",
      nazwa: "Pudding Chia",
      obrazek: "/sniadania/chia_pudding.jpg",
      cena: 15,
      opis: "Namoczone w mleku chia",
      dlugiopis:
        "Prosty i pyszny deser z namoczonych w mleku nasion CHIA. Do jego przygotowania można użyć dowolnego mleka (krowiego lub roślinnego) i dostępnych sezonowych owoców lub mango. Minimalny czas potrzebny do zagęszczenia puddingu chia to ok. 2h choć najlepsze efekty uzyskamy zostawiając go na noc w lodówce. Aby przyspieszyć ten proces dobrze jest wcześniej lekko podgrzać mleko. Konsystencja puddingu chia zależy od proporcji i rodzaju użytego mleka. Dla przykładu mleko kokosowe jest bardziej gęste niż krowie i można dać go więcej (nawet 400 ml jeśli mamy gęste mleko z puszki). Zbyt gęsty pudding można rozrzedzić dodając do niego niewielką ilość dodatkowego mleka.",
    },
    {
      number: 2,
      typ: "Śniadanie",
      nazwa: "Szakszuka",
      obrazek: "/sniadania/szakszuka_z_kurkami_0.jpg",
      cena: 23,
      opis: "Połączenie kurek i pomidorów",
      dlugiopis:
        "Sezonowa wersja szakszuki z leśnymi kurkami. Coś przepysznego! Połączenie kurek i pomidorów jest genialne. W szakszuce dodatek jajka ugotowanego w sosie pomidorowym daje śniadaniowy charakter tej potrawie. Jeszcze kromki dobrej jakości pieczywa i mamy pyszny oraz zdrowy posiłek gotowy.",
    },
    {
      number: 3,
      typ: "Śniadanie",
      nazwa: "Placuszki",
      obrazek: "/sniadania/placki_jogurtowe_z_bananem.jpg",
      cena: 16,
      opis: "Placuszki na jogurcie z bananem",
      dlugiopis:
        "Puszyste placuszki na jogurcie z plasterkami banana. Pyszne i łatwe do zrobienia z wykorzystaniem podstawowych składników, które na ogół zawsze mamy w kuchni. Z tego przepisu zrobicie oczywiście także klasyczne placki jogurtowe bez żadnych dodatków, wystarczy pominąć banana. Do podania proponuję syrop klonowy lub/i cukier puder.",
    },
    {
      number: 4,
      typ: "Śniadanie",
      nazwa: "Dutch Baby",
      obrazek: "/sniadania/dutch_baby_z_truskawkami.jpg",
      cena: 17,
      opis: "Pieczony naleśnik z truskawkami",
      dlugiopis:
        "Pieczony naleśnik (Dutch baby) to pyszna propozycja na śniadanie, lunch czy kolację. Na Kwestii Smaku od jakiegoś czasu cieszą się już popularnością przepisy na dutch baby ze śliwkami oraz z jabłkami, tym razem pora na truskawki z dodatkiem waniliowego serka homogenizowanego. Przepis wielokrotnie sprawdzony, polecam koniecznie do wypróbowania póki sezon na truskawki w pełni :-)",
    },
    {
      number: 5,
      typ: "Śniadanie",
      nazwa: "Omlet z dodatkiem",
      obrazek: "/sniadania/omlet_ze_szpinakiem.jpg",
      cena: 19,
      opis: "Omlet ze szpinakiem",
      dlugiopis:
        "Omlet ze szpinakiem to jeden z najczęściej przygotowywanych przeze mnie omletów. Można do niego dodać naprawdę dużo pysznego szpinaku :-) Dodatkowo do masy jajecznej ścieram trochę sera i kładę na wierzchu kilka połówek pomidorków koktajlowych. Jajka roztrzepuję z mlekiem, dzięki temu omlet nabiera puszystości. Po wylaniu masy jajeczno-szpinakowej na patelnię możemy ją przykryć i podgrzewać przez kilka minut na małym ogniu do ścięcia się masy. Równie często robię nieco inaczej, po 2 minutach smażenia masy jajecznej wkładam patelnię do piekarnika i zapiekam jeszcze chwilę do ścięcia się masy. Nieco więcej pracy ale wydaje mi się, że dzięki temu omlet jest delikatniejszy, ma jeszcze lepszą strukturę niż ten smażony pod przykryciem na kuchence.",
    },
    {
      number: 6,
      typ: "Śniadanie",
      nazwa: "Owocowa o wsianka",
      obrazek: "/sniadania/owsianka_z_owocami_jogurtem.jpg",
      cena: 13,
      opis: "Owsianka z dodatkami",
      dlugiopis:
        "Pyszna owsianka bez gotowania, z namaczanych gorącą wodą płatków owsianych, z dodatkiem jogurtu naturalnego. Równie smaczna jak tradycyjna owsianka gotowana na mleku, jednak dużo szybsza do przygotowania. Idealna na lato, gdy jest ciepło i nie mamy za bardzo ochoty na gotowanie i gorące posiłki. Płatki owsiane górskie zalane gorącą wodą dość szybko namakają. Już po kilku minutach pęcznieją, wówczas dodajemy do nich jogurt. Płatki osiągają też dzięki temu odpowiednią temperaturę. Potem wystarczy już tylko dodać owoce oraz syrop klonowy. Świetnym uzupełnieniem takiej owsianki są nasiona chia (namaczane razem z płatkami) i orzechy np. nerkowca, poeksperymentujcie aby znaleźć swoje ulubione dodatki.",
    },
  ]);
  const sniadania = sniadaniaLista.map((ele) => (
    <Grid item key={"gridSniadanie" + ele.number}>
      <Produkt
        key={"sniadanie" + ele.number}
        number={ele.number}
        typ={ele.typ}
        nazwa={ele.nazwa}
        obrazek={ele.obrazek}
        opis={ele.opis}
        dlugiopis={ele.dlugiopis}
        cena={ele.cena}
        dodaj={props.dodaj}
      />
    </Grid>
  ));

  return (
    <Container>
      <Grid container direction="row" justify="space-around">
        {sniadania}
      </Grid>
    </Container>
  );
}

//komponent wyświetlający produkty z obiadami
function Obiady(props) {
  const [obiadyLista, setobiadyLista] = React.useState([
    {
      number: 1,
      typ: "Obiad",
      nazwa: "Pulpeciki",
      obrazek: "/obiady/pulpeciki-w-sosie-carbonara.jpg",
      cena: 25,
      opis: "Pulpeciki w sosie carbonara",
      dlugiopis:
        "Danie na rodzinny obiad, który można z powodzeniem przygotować z wyprzedzeniem, np. poprzedniego dnia. Pulpeciki dobrze znoszą odgrzewanie, można je zawekować na kilka dni. Inspiracją do ich przygotowania jest włoski sos carbonara z dodatkiem podsmażonego boczku. Oprócz niego dodajemy też natkę pietruszki oraz oczywiście śmietankę. ",
    },
    {
      number: 2,
      typ: "Obiad",
      nazwa: "Pieczone kaczki",
      obrazek: "/obiady/pieczone-udka-kaczki-z-pomaranczami.jpg",
      cena: 23,
      opis: "Udka kaczki z pomarańczami",
      dlugiopis:
        "Oprócz pieczonych udek kaczki z dodatkiem jabłek i żurawiny, równie popularna jest kaczka z pomarańczami. Może nawet bardziej pasuje na Świąteczny obiad... Możecie tutaj użyć zarówno udek jak i ćwiartek kaczki, przepis różnić się będzie tylko czasem pieczenia. Oprócz pomarańczy smaku dodaje rozmaryn oraz czerwona cebula.",
    },
    {
      number: 3,
      typ: "Obiad",
      nazwa: "Naleśniki mięsne",
      obrazek: "/obiady/nalesniki-po-bolonsku_0.jpg",
      cena: 20,
      opis: "Naleśnik po bolońsku",
      dlugiopis:
        "Naleśniki przekładane nadzieniem mięsnym i zapiekane pod sosem beszamelowym - to potrawa w klimacie włoskiej LASAGNE BOLOGNESE tylko z wykorzystaniem naszych polskich naleśników :) Myślę, że wszyscy fani włoskiej kuchni (i nie tylko oni) będą zadowoleni :) ",
    },
    {
      number: 4,
      typ: "Obiad",
      nazwa: "Gulasz",
      obrazek: "/obiady/gulasz_z_indyka_z_kurkami.jpg",
      cena: 26,
      opis: "Gulasz z indyka z kurkami",
      dlugiopis:
        "Smakowity gulasz na rodzinny obiad, dobry również do odgrzewania następnego dnia. Filet z indyka, do tego pyszne sezonowe kurki, cebula, por i marchewka. Wszystko w delikatnym sosie ze śmietanką. Z dodatków pasować będą ziemniaki, ale sprawdzi się również bulgur oraz kasza gryczana czy jęczmienna. Warto wypróbować póki sezon grzybowy w pełni ;-)",
    },
    {
      number: 5,
      typ: "Obiad",
      nazwa: "Cannelloni",
      obrazek: "/obiady/cannelloni_ze_szpinakiem.jpg",
      cena: 29,
      opis: "Cannelloni ze szpinakiem",
      dlugiopis:
        "Dzisiejszy przepis jest w klasycznej wersji z makaronem cannelloni w kształcie rurek. I jest dużo łatwiejszy do przygotowania! Nie dość bowiem, że nie trzeba smażyć naleśników, bo kupujemy gotowy makaron, to nawet nie trzeba go gotować. Surowy makaron nadziewamy farszem i pieczemy pod przykryciem z sosu pomidorowego, którym się ugotuje. Koniecznie do wypróbowania!",
    },
    {
      number: 6,
      typ: "Obiad",
      nazwa: "Schab",
      obrazek: "/obiady/schab_w_sosie_koperkowym.jpg",
      cena: 21,
      opis: "Schab w sosie koperkowym",
      dlugiopis:
        "Pomysł na pyszny obiad dla całej rodziny! Delikatny i kruchy schab w śmietankowym sosie z dodatkiem marchewki i koperku. Do tego młode ziemniaczki oraz surówka z zielonej kapusty i pomidora. Polecam koniecznie do wypróbowania :-)",
    },
  ]);

  const obiady = obiadyLista.map((ele) => (
    <Grid item key={"gridObiady" + ele.number}>
      <Produkt
        key={"obiad" + ele.number}
        number={ele.number}
        typ={ele.typ}
        nazwa={ele.nazwa}
        obrazek={ele.obrazek}
        opis={ele.opis}
        dlugiopis={ele.dlugiopis}
        cena={ele.cena}
        dodaj={props.dodaj}
      />
    </Grid>
  ));
  return (
    <Container>
      <Grid container direction="row" justify="space-around">
        {obiady}
      </Grid>
    </Container>
  );
}

//komponent wyświetlający produkty z koktajlami
function Koktajle(props) {
  const [koktajleLista, setkoktajleLista] = React.useState([
    {
      number: 1,
      typ: "Koktajl",
      nazwa: "Lemoniada",
      obrazek: "/koktajle/lemoniada_imbirowa.jpg",
      cena: 12,
      opis: "Lemoniada z imbirem",
      dlugiopis:
        "Lemoniada, którą często robię w czasie jesienno-zimowym, ze względu na dodatek świeżego imbiru, który ma działanie rozgrzewające, wspomagające odporność i zwalczające infekcje. O tej porze roku pod dostatkiem mamy też bogate w witaminę C owoce cytrusowe jak cytryny, limonki i pomarańcze. Taką lemoniadę można też zrobić na imprezę podczas karnawału.",
    },
    {
      number: 2,
      typ: "Koktajl",
      nazwa: "Kompot",
      obrazek: "/koktajle/kompot.jpg",
      cena: 18,
      opis: "Kompot",
      dlugiopis:
        "Klasyczny przepis na najlepszy, domowy kompot - smak dzieciństwa i smak lata :-) Możemy dodawać do niego różne owoce, ale warto dorzucić czerwone maliny czy porzeczki aby podkręcić kolor. W sierpniu smakuje naprawdę wyjątkowo, z młodymi jabłkami, gruszkami i śliwkami. Po ugotowaniu kompot dobrze jest odstawić na kilkanaście minut aby trochę się przestudził i nabrał smaku.",
    },
    {
      number: 3,
      typ: "Koktajl",
      nazwa: "Czekolada",
      obrazek: "/koktajle/czekolada_na_goraco_na_czerwonym_winie.jpg",
      cena: 19,
      opis: "Czekolada na gorąco na czerwonym winem",
      dlugiopis:
        "Gorąca czekolada to wspaniała nagroda na zakończenie długiego, zimowego spaceru :-) Rozgrzewa i poprawia nastrój. Znamy już przepis na klasyczną czekoladę na gorąco, na czekoladę w stylu meksykańskim z chili i cynamonem, a dzisiaj kolejna propozycja nie do odrzucenia - czekolada na gorąco na czerwonym winie ;-) Nie da się opisać tego smaku, po prostu idealnie uzupełniające się składniki. Polecam na wieczór przy kominku czy telewizji, na spotkanie ze znajomymi czy romantyczny wieczór we dwoje.",
    },
    {
      number: 4,
      typ: "Koktajl",
      nazwa: "Poncz",
      obrazek: "/koktajle/poncz_swiateczny.jpg",
      cena: 11,
      opis: "Poncz świąteczny",
      dlugiopis:
        "Co podać na Święta do picia oprócz kompotu z suszu? Moja propozycja to świąteczny poncz z czerwonego wina i herbaty z dodatkiem przypraw korzennych, pomarańczy i cytryny. Można go podać na ciepło lub na zimno, np. w wazie lub dzbanku. Są różne rodzaje ponczu, z dodatkiem różnych owoców i alkoholi (np. na bazie rumu lub wódki). Poncz pochodzi z Indii, obecnie popularny jest np. w Wielkiej Brytanii, Włoszech, Hiszpanii i Francji. Oryginalnie przygotowywano go z pięciu składników: herbaty, cukru, cytryny, jakiegoś owocu i wina.",
    },
    {
      number: 5,
      typ: "Koktajl",
      nazwa: "Latte",
      obrazek: "/koktajle/czekolada_na_goraco_na_czerwonym_winie.jpg",
      cena: 15,
      opis: "Pierniczkowa latte",
      dlugiopis:
        "Pierniczkowa latte czyli pyszna trójwarstwowa kawa o słodkim smaku piernikowym. Aby ją zrobić wystarczy podgrzać niewielką ilość mleka zagęszczonego (7,5%, do kawy) razem z odrobiną przyprawy piernikowej i cukru, następnie uzupełnić mocnym espresso i dobrze spienionym mlekiem. W mojej kuchni od lat sprawdza się tutaj prosty spieniacz do mleka Bialletti, ubite nim mleko potrafi być gęste niemalże jak bita śmietana :-) Polecam na zimowe poranki!",
    },
    {
      number: 6,
      typ: "Koktajl",
      nazwa: "Chai",
      obrazek: "/koktajle/chai.jpg",
      cena: 23,
      opis: "Indyjska herbata",
      dlugiopis:
        "Doprawiona ostrymi i aromatycznymi przyprawami, posłodzona i zaparzana z mlekiem herbata to indyjski Masala Chai, lub po prostu Chai. Rozgrzewa i — dostarczając też całkiem niezłą dawkę kofeiny — stawia na nogi niemalże jak kawa. Muszę przyznać, że o tej porze roku to moje uzależnienie i prawie codziennie przygotowuję Chai zamiast zwykłej herbaty :-)",
    },
  ]);

  const koktajle = koktajleLista.map((ele) => (
    <Grid item key={"gridKoktajle" + ele.number}>
      <Produkt
        key={"koktajl" + ele.number}
        number={ele.number}
        typ={ele.typ}
        nazwa={ele.nazwa}
        obrazek={ele.obrazek}
        opis={ele.opis}
        dlugiopis={ele.dlugiopis}
        cena={ele.cena}
        dodaj={props.dodaj}
      />
    </Grid>
  ));
  return (
    <Container>
      <Grid container direction="row" justify="space-around">
        {koktajle}
      </Grid>
    </Container>
  );
}
