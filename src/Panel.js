/**
 * Komponent Panel ma za zadanie zasymulować 'pseudo panel administratora' strony. Bez dodatkowych ingerencji w zabezpieczenia
 * oraz autoryzujące panele logowania, ładujemy dynamicznie do widoku komponentu dane z bazy danych, tabela zamowienia i 
 * formularze celem sprezentowania danych.
 */
import React, { Component } from 'react';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import Badge from '@material-ui/core/Badge';
import Modal from '@material-ui/core/Modal';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import CloseIcon from '@material-ui/icons/Close';
import TableFooter from '@material-ui/core/TableFooter';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';

function rand() {
	return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
	const top = 0;
	const right = 0;

	return {
		top: `${top}%`,
		right: `${right}%`,
		transform: `translate(-${top}%, -${right}%)`,
		overflow: 'scroll',
		height: '100%',
		display: 'block'
	};
}

const useStyles = makeStyles((theme) => ({
	paper: {
		position: 'absolute',
		width: '100%',
		backgroundColor: theme.palette.background.paper,
		border: '2px solid #000',
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3)
	}
}));

export default function Panel() {
	const classes = useStyles();
	const [ modalStyle ] = React.useState(getModalStyle);
	const [ open, setOpen ] = React.useState(false);

	const handleGET = () => {
		let dane = [];

		// async function getData(url = 'http://localhost:3001/getDataZamowienia') {
		// 	// Default options are marked with *
		// 	const response = await fetch(url, {
		// 		method: 'POST', // *GET, POST, PUT, DELETE, etc.
		// 		mode: 'cors',
		// 		headers: {
		// 			'Content-Type': 'application/json'
		// 		}
		// 		// body: JSON.stringify(data) // body data type must match "Content-Type" header
		// 	});

		// 	return console.log(response); // parses JSON response into native JavaScript objects
		// }

		fetch('http://localhost:3001/getDataZamowienia')
			.then((response) => response.json())
			.then((data) => console.log(data));

		// getData();
	};

	const handleOpen = () => {
		setOpen(true);
		handleGET();
	};

	const handleClose = () => {
		setOpen(false);
	};

	const handleClear = () => {
		setOpen(false);
	};

	const body = (
		<div style={modalStyle} className={classes.paper}>
			<h2 id="simple-modal-title">Panel administratora:</h2>
			<CloseIcon onClick={handleClose} style={{ position: 'absolute', top: 10, right: 10, cursor: 'pointer' }} />
			<TableContainer component={Paper}>
				<Table className={classes.table} aria-label="simple table">
					<TableHead>
						<TableRow>
							<TableCell align="left">Nazwa</TableCell>

							<TableCell align="right">Typ</TableCell>
							<TableCell align="right">Cena</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						<TableRow key={'123'}>
							<TableCell align="left" />
							<TableCell align="right" />
							<TableCell align="right" />
						</TableRow>
					</TableBody>
				</Table>
			</TableContainer>
		</div>
	);

	return (
		<div>
			<React.Fragment>
				<IconButton
					onClick={handleOpen}
					style={{ position: 'fixed', right: '0px', top: '0px' }}
					color="default"
					aria-label="shop"
					component="span"
				>
					<Badge badgeContent={4} color={'secondary'}>
						<SupervisorAccountIcon />
					</Badge>
				</IconButton>
			</React.Fragment>
			<Modal
				open={open}
				onClose={handleClose}
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
			>
				{body}
			</Modal>
		</div>
	);
}
