var express = require('express');
var router = express.Router();
var pgp = require('pg-promise')(/* options */);
var db = pgp('postgres://postgres:1234@localhost:5432/postgres');

db
	.one('SELECT $1 AS value', 'pg-promise działa!')
	.then(function(data) {
		console.log('DATA:', data.value);
	})
	.catch(function(error) {
		console.log('ERROR:', error);
	});

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', { title: 'Express' });
});

//GET
router.get('/getDataZamowienia', function(req, res) {
	db
		.one('SELECT * FROM zamowienia')
		.then((data) => {
			console.log(data);
			res.send(data);
		})
		.catch((error) => {
			console.log(error);
		});
});

//POST
router.post('/postDataZamowienia', function(req, res) {
	console.log(' ');
	console.log('Zamówienie otrzymane!');
	res.send(req.body);

	let rekord = req.body;
	let suma = 0;
	rekord[0].map((ele) => {
		suma += ele.cena;
	});
	let zamowienieLista = rekord[0].map((ele) => {
		return ele.typ + ' nr.' + ele.number + ' - ' + ele.nazwa;
	});

	console.log('Wprowadzono zamówienie do bazy: ', zamowienieLista, ' od ', rekord[1].imieinazwisko);
	db
		.one(
			'INSERT INTO zamowienia(imienazwisko, email, telefon, suma, zamowienie, adres) VALUES($1, $2, $3, $4, $5, $6)',
			[ rekord[1].imieinazwisko, rekord[1].email, rekord[1].telefon, suma, zamowienieLista, rekord[1].adres ]
		)
		.then((x) => {
			// success;
			console.log('DONE!', x);
		})
		.catch((error) => {
			// error;
		});
});

//POST
router.post('/postDataZgloszenia', function(req, res) {
	console.log(' ');
	console.log('Zgloszenie otrzymane!');
	res.send(req.body);

	console.log('Wprowadzono zamówienie do bazy: ', req.body);
	db
		.one('INSERT INTO formularze(imie, telefon, email, wiadomosc) VALUES($1, $2, $3, $4)', [
			req.body.imieinazwisko,
			req.body.telefon,
			req.body.email,
			req.body.dodatkoweinfo
		])
		.then((x) => {
			// success;
			console.log('DONE!', x);
		})
		.catch((error) => {
			// error;
		});
});

module.exports = router;
