createdb catering

///////////////////////////////////////

CREATE TABLE public.zamowienia
(
    id integer NOT NULL DEFAULT nextval('zamowienia_id_seq'::regclass),
    imienazwisko text COLLATE pg_catalog."default",
    email text COLLATE pg_catalog."default",
    telefon text COLLATE pg_catalog."default",
    suma bigint,
    zamowienie text[] COLLATE pg_catalog."default",
    CONSTRAINT zamowienia_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.zamowienia
    OWNER to postgres;

/////////////////////////////////////

    CREATE TABLE public.formularze
(
    id integer NOT NULL DEFAULT nextval('formularze_id_seq'::regclass),
    imie text COLLATE pg_catalog."default",
    telefon text COLLATE pg_catalog."default",
    email text COLLATE pg_catalog."default",
    wiadomosc text COLLATE pg_catalog."default",
    CONSTRAINT formularze_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.formularze
    OWNER to postgres;



////////////////////////////////////////



INSERT INTO public.zamowienia(
	imienazwisko, email, telefon, suma, zamowienie)
	VALUES ('Romek Atomek', 'ra@tek.ek', '666', '6666', '{"23123ewe,1235w,123123", "12312ead,1235t,123123"}');